﻿using System;
using System.Collections.Generic;
using System.Text;
using RestSharp;
using APIPicoPlacaSerna.Models;
using Newtonsoft.Json.Linq;

namespace APIPicoPlacaSerna.Controllers
{
    class gestorVehiculo
    {
        public InfoVehiculo ConsultarDias(string placa)
        {
            InfoVehiculo objVehiculo = new InfoVehiculo();

            var client = new RestClient("http://demo3162273.mockable.io/picoplaca");
            var request = new RestRequest(Method.GET);
            IRestResponse response = client.Execute(request);

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                JObject jsonInfoVehiculo = JObject.Parse(response.Content.ToString());

                objVehiculo.placa = placa;

                string digitoCarro = placa.Substring(placa.Length - 1);
                string digitoMoto = placa.Substring(placa.Length - 3, 1);
                string dias;

                bool canConvertCarro = Int32.TryParse(digitoCarro, out int n1);

                if (canConvertCarro == true)
                {
                    dias = jsonInfoVehiculo["carro"][digitoCarro].ToString();
                }
                else
                {
                    dias = jsonInfoVehiculo["moto"][digitoMoto].ToString();
                }

                string[] arrayDias = dias.Replace("[", "").Replace("]", "").Replace("\"", "").Replace(" ", "").Replace("\n", "").Split(',');

                foreach (string miItem in arrayDias)
                {
                    objVehiculo.dias.Add(miItem);
                    Console.WriteLine(miItem);
                }
            }
            return objVehiculo;
        }
    }
}