﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using APIPicoPlacaSerna.Controllers;
using System.Globalization;

namespace APIPicoPlacaSerna
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private void ConsultarDias(object sender, EventArgs e)
        {
            gestorVehiculo objVehiculo = new gestorVehiculo();

            int n1 = 0;

            if (txtPlaca.Text != null && txtPlaca.Text.Length == 6)
            {
                string d1 = txtPlaca.Text.Substring(0, 1);
                string d2 = txtPlaca.Text.Substring(1, 1);
                string d3 = txtPlaca.Text.Substring(2, 1);
                string d4 = txtPlaca.Text.Substring(3, 1);
                string d5 = txtPlaca.Text.Substring(4, 1);

                bool con1 = Int32.TryParse(d1, out n1);
                bool con2 = Int32.TryParse(d2, out n1);
                bool con3 = Int32.TryParse(d3, out n1);
                bool con4 = Int32.TryParse(d4, out n1);
                bool con5 = Int32.TryParse(d5, out n1);

                if (con1 == false && con2 == false && con3 == false && con4 == true && con5 == true)
                {
                    Models.InfoVehiculo infoVehiculo = objVehiculo.ConsultarDias(txtPlaca.Text);
                    string cadena = "";

                    CultureInfo ci = new CultureInfo("Es-Es");
                    string hoy = ci.DateTimeFormat.GetDayName(DateTime.Now.DayOfWeek);
                    bool pp = false;

                    foreach (var item in infoVehiculo.dias)
                    {
                        cadena = cadena + item.ToString() + "\n";
                        lstDias.ItemsSource = infoVehiculo.dias;
                        lstDias.IsVisible = true;

                        if (item.ToString() == hoy)
                        {
                            pp = true;
                        }
                    }

                    if (pp == true)
                    {
                        lblAdvertencia.BackgroundColor = Color.Red;
                        lblAdvertencia.Text = "ADVERTENCIA: Hoy tienes pico y placa.";
                    }
                    else
                    {
                        lblAdvertencia.BackgroundColor = Color.LimeGreen;
                        lblAdvertencia.Text = "Hoy no tienes pico y placa, maneja con precaucion.";
                    }

                    btnPlaca.Text = txtPlaca.Text;
                    btnPlaca.IsVisible = true;

                    lblAdvertencia.IsVisible = true;

                    lblTusDias.Text = "Tus dias pico y placa son:";
                    lblTusDias.IsVisible = true;

                    txtPlaca.IsVisible = false;
                    btnConsultar.IsVisible = false;

                    btnNuevo.IsVisible = true;

                }
                else
                {
                    DisplayAlert("Error", "Placa invalida \nPorfavor digite una placa valida", "OK");
                }
            }
            else
            {
                DisplayAlert("Error", "Placa invalida \nPorfavor digite una placa valida", "OK");
            }
        }

        private void NuevaConsulta(object sender, EventArgs e)
        {
            txtPlaca.Text = "";
            txtPlaca.IsVisible = true;
            btnConsultar.IsVisible = true;
            btnNuevo.IsVisible = false;
            btnPlaca.IsVisible = false;
            lblAdvertencia.IsVisible = false;
            lblTusDias.IsVisible = false;
            lstDias.IsVisible = false;
        }

    }
}
