﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APIPicoPlacaSerna.Models
{
    class InfoVehiculo
    {
        public string placa { get; set; }
        public List<string> dias = new List<string>();
    }
}
